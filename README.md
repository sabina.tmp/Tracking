# Test

## Specs:

Please implement a system where people can enter a shipping tracking code in a form
 and as a result they will see an estimated delivery date.
The frontend is supposed to be connecting to the backend through a REST API.
Please make sure that the data store in the backend can be replaced (e.g by a config).
Implement two storage solutions that can be plugged in.
Suggestions for data stores to implement are SQLite and file (CSV).
Feel free to use libraries, when it makes sense.
Please be careful about unit tests, commits and all those other pesky details. They matter.
Deliver the results as a git repository with commit history.
The implementation should be done in a half day (4h).


https://laravel.com/docs/5.3/seeding

## TODO:
- Storage service provider
- storage type interface
- storage facade
- tests
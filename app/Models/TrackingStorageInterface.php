<?php

namespace App\Models;

interface TrackingStorageInterface {

    public function getRow($id);
    
    public function getEstimatedDelivery($id);

}
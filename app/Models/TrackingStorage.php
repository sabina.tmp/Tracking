<?php

namespace App\Models;

class TrackingStorage {

    public $modelClass;

    /**
     * TrackingStorage constructor.
     */
    public function __construct(CSVTrackingStorage $csvModel, SQLiteTrackingStorage $sqliteModel)
    {
        if (config('app.storage') == 'csv') {
            $this->modelClass = $csvModel;
        } else {
            $this->modelClass = $sqliteModel;
        }
    }

    public function getRow($id) {
        return $this->modelClass->getRow($id);
    }

    public function getEstimatedDelivery($id) {
        return $this->modelClass->getEstimatedDelivery($id);
    }


}
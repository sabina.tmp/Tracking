<?php

namespace App\Models;

class CSVTrackingStorage implements TrackingStorageInterface {

    protected $fileName;

    protected $data;

    /**
     * CSVTrackingStorage constructor.
     *
     * Load data from file here
     */
    public function __construct()
    {
        $this->fileName = config('database.csvFile');

        if (($handle = fopen($this->fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $this->data[$data[0]] = array(
                    'id' => $data[0],
                    'name' => $data[1],
                    'address' => $data[2],
                    'city' => $data[3],
                    'country' => $data[4],
                    'estimatedDelivery' => $data[5],
                );
            }

            fclose($handle);
        }
    }

    public function getRow($id) {
        return $this->data[$id];
    }

    public function getEstimatedDelivery($id) {
        $row = $this->getRow($id);

        return $row['estimatedDelivery'];
    }

}
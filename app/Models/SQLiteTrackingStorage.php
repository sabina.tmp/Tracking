<?php

namespace App\Models;

class SQLiteTrackingStorage implements TrackingStorageInterface {

    /**
     * TrackingStorage constructor.
     */
    public function __construct()
    {
    }

    public function getRow($id) {

        $query = DB::select()
            ->where("id = :uid,", array('id' => $id));

        $row = $query->first();

        return $row;
    }

    public function getEstimatedDelivery($id){
        $row = $this->getRow($id);

        return $row['estimatedDelivery'];
    }

}
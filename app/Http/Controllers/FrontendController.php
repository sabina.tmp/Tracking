<?php

namespace App\Http\Controllers;

use App\Facades\TrackingStorageFacade;
use App\Models\CSVTrackingStorage;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View
     */
    public function index()
    {
        return view('frontend.index');
    }

    
}

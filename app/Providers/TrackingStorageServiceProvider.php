<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\CSVTrackingStorage;
use App\Models\SQLiteTrackingStorage;

class TrackingStorageServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $csv = new CSVTrackingStorage();
        $this->app->bind('CSVTrackingStorage', function() use ($csv) {
            return $csv;
        });

        $sqlite = new SQLiteTrackingStorage();
        $this->app->bind('SQLiteTrackingStorage', function() use ($sqlite) {
            return $sqlite;
        });

        $this->app->bind('TrackingStorage', function() use ($csv, $sqlite) {
            return new App\Models\TrackingStorage($csv, $sqlite);
        });

    }
}
<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrackingControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShow()
    {
        $this->json('GET', route('tracking.show', ['tracking' => '1']))
            ->seeJsonStructure([
                'id',
                'name',
                'address',
                'city',
                'country',
                'estimatedDelivery',
            ]);
    }
}

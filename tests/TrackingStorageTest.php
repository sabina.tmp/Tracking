<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\CSVTrackingStorage;
use App\Models\SQLiteTrackingStorage;
use App\Models\TrackingStorage;


class TrackingStorageTest extends TestCase
{
    protected $csv;
    protected $sqlite;

    public function setUp()
    {
        parent::setUp();
        $this->csv = \Mockery::mock('App\Models\CSVTrackingStorage');
        $this->sqlite = \Mockery::mock('App\Models\SQLiteTrackingStorage');
    }

    /**
     * @return void
     */
    public function testModelClassCSV()
    {
        config(['app.storage' => 'csv']);

        $tracking = \Mockery::mock('App\Models\TrackingStorage', $this->csv, $this->sqlite);

        $this->assertInstanceOf('App\Models\CSVTrackingStorage', $tracking->modelClass);
    }

    /**
     * @return void
     */
    public function testModelClassSQLite()
    {
        config(['app.storage' => 'sqlite']);

        $tracking = \Mockery::mock('App\Models\TrackingStorage', $this->csv, $this->sqlite);

        $this->assertInstanceOf('App\Models\SQLiteTrackingStorage', $tracking->modelClass);
    }


    /**
     * @return void
     */
    public function testGetRow()
    {
        $tracking = \Mockery::mock('App\Models\TrackingStorage', $this->csv, $this->sqlite);
        $row = $tracking->getRow('1');

        $this->assertTrue($row['id'] == 1);
    }

    /**
     * @return void
     */
    public function testGetEstimatedDelivery()
    {
        $tracking = \Mockery::mock('App\Models\TrackingStorage', $this->csv, $this->sqlite);
        $dateString = $tracking->getEstimatedDelivery('1');
        $date = new \DateTime($dateString);

        $this->assertInstanceOf('DateTime', $date);
    }
}

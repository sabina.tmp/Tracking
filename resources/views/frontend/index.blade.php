@extends('layout')

@section('content')
    <form action="{{ route('tracking.show', ['tracking' => 'id']) }}" class="m-b-md">
        <div class="form-group row">
            <label for="example-text-input" class="col-xs-2 col-form-label">Code</label>
            <div class="col-xs-10">
                <input required class="form-control" name="id"
                       placeholder="Please enter your tracking code"
                       type="text" value="" id="trackin-code">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <div class="results"></div>
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            var $form = $('form');
            var baseUrl = $form.attr('action');
            var $results = $('.results');

            $form.submit(function(ev){
                ev.preventDefault();

                var url = baseUrl.replace('id', $form.find('input').val());
                $.get(url, function(data){
                    $results.html('Estimated delivery date is: ' + data.estimatedDelivery);
                });

            });

        });
    </script>
@endsection
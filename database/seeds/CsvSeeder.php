<?php

use Illuminate\Database\Seeder;

class CsvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fileName = config('database.csvFile');

        $fp = fopen($fileName, 'w');
        for ($i = 1; $i <= 50; $i++) {

            $int = mt_rand(1479170294,1481762294); // rand between 15 Nov & 15 Dec
            $date = date("Y-m-d H:i:s", $int);
            
            $fields = [
                'id' => $i,
                'name' => str_random(10),
                'address' => str_random(20),
                'city' => str_random(10),
                'country' => str_random(10),
                'estimatedDelivery' => $date,
            ];

            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
}
